# keylogger

Le but de ce projet était de créer un keylogger logiciel fonctionnel. Il permet de récupérer et d’enregistrer les frappes de touches du clavier à l’insu de l’utilisateur.

----------------------------------------------------------------------------------------------------------------------------------------
UTILISATION

Python 2.7


----------------------------------------------------------------------------------------------------------------------------------------
PAQUETS UTILISES

Paquets natifs: OS<br>
Autres: Socket, Subprocess


----------------------------------------------------------------------------------------------------------------------------------------
PREREQUIS

VMware
https://fr.wikihow.com/installer-VMware-Workstation-et-cr%C3%A9er-une-machine-virtuelle-sur-un-ordinateur

Kali Linux
https://www.kali-linux.fr/installation/installer-kali-linux-en-francais

Notions en python
https://openclassrooms.com/courses/apprenez-a-programmer-en-python/qu-est-ce-que-python

Notions sur les sockets
https://openclassrooms.com/courses/introduction-aux-sockets-1


----------------------------------------------------------------------------------------------------------------------------------------
APPELATION DES FICHIERS

Serv.py (Victime)<br>
Script.py (Attaquant)<br>
Keylogger.py (Script pour le keylogger)<br>
Pyxhook.py (Module linux permettant au keylogger de fonctionner)<br>
File.log (Fichier où sont stockées les données de l’utilisateur)<br>


----------------------------------------------------------------------------------------------------------------------------------------
INSTALLATION

Création des sockets et connexion entre le serveur et le client

Les sockets sont utilisés pour communiquer entre deux hôtes que l’on nomme traditionnellement Client/ Serveur à l’aide d’une adresse IP et d’un port.
- importation des librairies sys, socket, et os,
- Adresse IP serveur « 192.168.10.128 »,
- Port 1234.

 
 
Exécution des commandes à distance 
 
Le client exécute une commande, par exemple IP config. Le résultat s’affiche de son côté et le serveur reçoit un message comme quoi la commande est bien passée, sinon nous affichons « Commande inconnue au bataillon ».
Le client exécute une fonction via raw_input(« »).
Le serveur reçoit la commande avec la commande cmd = client.recv(4000)




Téléchargement de fichiers

- Connexion entre le serveur et le client grâce aux sockets
- Le client demande à télécharger le fichier1 :
o Le client vérifie si le fichier1 n’existe pas
o Le serveur vérifie si fichier1 existe
- Le serveur ouvre le fichier et stocke son contenu dans une variable
- Le serveur envoie la variable où le contenu est stocké au client
- Le client récupère la variable 
- Le client ouvre un fichier portant le nom fichier1 et y écrit le contenu de la variable




Upload de fichiers

Meme principe que le téléchargement sauf que le fichier sera stocké sur le serveur.






Keylogger

Le keylogger s'execute a l'aide du fichier pyxhook.py et du fichier keylogger.py
On doit imperativement installer le package python-xlib et git
On envoi les fichiers keylogger.py et pyhook.py sur la machine victime puis on l'execute
On obtient un fichier .log sur la machne de la victime de toutes les touches du clavier tapées.
On recupere ensuite le fichier log.
L'analyse se termine une fois que l'on a tapé sur la touche "=".

----------------------------------------------------------------------------------------------------------------------------------------

Auteurs:<br>
• Suiro Quentin – Alternant chez Thales / Etudiant CFI Montigny (MSIR)<br>
• Rose Jordan – Alternant à l’IRBA / Etudiant CFI Montigny (MSIR)<br>
• Rose Vincent – Alternant à l’IRBA / Etudiant CFI Montigny (MSIR)<br>







